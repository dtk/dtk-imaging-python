# Changelog
## version 2.0.6 - 2020-02-27
- fix compilation on windows
## version 2.0.5 - 2020-02-27
- fix install on windows
## version 2.0.4 - 2019-09-25
- rebuild to match dtk-imaging
## version 2.0.3 - 2019-09-24
- enhance wrapping of dtkImage
## version 2.0.2 - 2019-09-20
- initial release
