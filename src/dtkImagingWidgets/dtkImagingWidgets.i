// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

%module(directors="1") dtkimagingwidgets

%include <dtkBase/dtkBase.i>

%import  <dtkCore/dtkCore.i>

%{
#include <dtkAbstractImageViewer.h>
%}

%feature("director");

%extend QVariant {
    void setValue(dtkImage *value) {
        $self->setValue(dtkMetaType::variantFromValue(value));
    }
    dtkImage *todtkImage() const {
        return $self->value<dtkImage *>();
    }
};

// /////////////////////////////////////////////////////////////////
// Macro undefinition
// /////////////////////////////////////////////////////////////////

#undef  DTKIMAGINGWIDGETS_EXPORT
#define DTKIMAGINGWIDGETS_EXPORT

// /////////////////////////////////////////////////////////////////
// Wrapper input
// /////////////////////////////////////////////////////////////////

%include <dtkAbstractImageViewer.h>

//
// dtkImagingWidgets.i.in ends here
