/* Version: $Id$
 *
 */

/* Commentary:
 *
 */

/* Change Log:
 *
 */

/* Code: */

#pragma once

%module(directors="1") dtkimagingfilters

%include <dtkBase/dtkBase.i>

%import  <dtkCore/dtkCore.i>

%{

#include <dtkImaging.h>
#include <dtkImagingSettings.h>
#include <dtkImage.h>
#include <dtkImageConverter.h>
#include <dtkImageReader.h>
#include <dtkImageWriter.h>

#include <dtkAbstractGaussianFilter.h>
#include <dtkAbstractHTransformFilter.h>
#include <dtkAbstractConnectivityThreshold.h>
#include <dtkAbstractXorFilter.h>
#include <dtkAbstractConnectedComponentsFilter.h>
#include <dtkAbstractDilateFilter.h>
#include <dtkAbstractMedianFilter.h>
#include <dtkAbstractCloseFilter.h>
#include <dtkAbstractWatershedFilter.h>
#include <dtkAbstractShrinkFilter.h>
#include <dtkAbstractMeshICP.h>
#include <dtkAbstractThresholdFilter.h>
#include <dtkAbstractNormalizeFilter.h>
#include <dtkAbstractSubtractFilter.h>
#include <dtkAbstractDistanceMapFilter.h>
#include <dtkAbstractWindowingFilter.h>
#include <dtkAbstractMaskApplicationFilter.h>
#include <dtkAbstractErosionFilter.h>
#include <dtkAbstractAddFilter.h>
#include <dtkAbstractErodeFilter.h>
#include <dtkAbstractSegmentation.h>
#include <dtkAbstractMeshingFilter.h>
#include <dtkAbstractDivideFilter.h>
#include <dtkAbstractInvertFilter.h>
#include <dtkAbstractImageSubtraction.h>
#include <dtkAbstractMultiplyFilter.h>
#include <dtkAbstractOpenFilter.h>
#include <dtkAbstractImageMeshMapping.h>

%}

%feature("director");

%extend QVariant {
    void setValue(dtkImage *value) {
        $self->setValue(dtkMetaType::variantFromValue(value));
    }
    dtkImage *todtkImage() const {
        return $self->value<dtkImage *>();
    }
};

// /////////////////////////////////////////////////////////////////
// Macro undefinition
// /////////////////////////////////////////////////////////////////

#undef  DTKIMAGINGFILTERS_EXPORT
#define DTKIMAGINGFILTERS_EXPORT

// /////////////////////////////////////////////////////////////////
// Wrapper input
// /////////////////////////////////////////////////////////////////

%include <dtkAbstractGaussianFilter.h>
%include <dtkAbstractHTransformFilter.h>
%include <dtkAbstractConnectivityThreshold.h>
%include <dtkAbstractXorFilter.h>
%include <dtkAbstractConnectedComponentsFilter.h>
%include <dtkAbstractDilateFilter.h>
%include <dtkAbstractMedianFilter.h>
%include <dtkAbstractCloseFilter.h>
%include <dtkAbstractWatershedFilter.h>
%include <dtkAbstractShrinkFilter.h>
%include <dtkAbstractMeshICP.h>
%include <dtkAbstractThresholdFilter.h>
%include <dtkAbstractNormalizeFilter.h>
%include <dtkAbstractSubtractFilter.h>
%include <dtkAbstractDistanceMapFilter.h>
%include <dtkAbstractWindowingFilter.h>
%include <dtkAbstractMaskApplicationFilter.h>
%include <dtkAbstractErosionFilter.h>
%include <dtkAbstractAddFilter.h>
%include <dtkAbstractErodeFilter.h>
%include <dtkAbstractSegmentation.h>
%include <dtkAbstractMeshingFilter.h>
%include <dtkAbstractDivideFilter.h>
%include <dtkAbstractInvertFilter.h>
%include <dtkAbstractImageSubtraction.h>
%include <dtkAbstractMultiplyFilter.h>
%include <dtkAbstractOpenFilter.h>
%include <dtkAbstractImageMeshMapping.h>

/* dtkImagingFilters.i ends here */
