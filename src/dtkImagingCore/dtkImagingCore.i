// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

%module(directors="1") dtkimagingcore

%include <dtkBase/dtkBase.i>

%import  <dtkCore/dtkCore.i>

%{

#define SWIG_FILE_WITH_INIT

#include <dtkCoreMetaType.h>
#include <dtkImaging.h>
#include <dtkImagingPixel.h>
#include <dtkImagingTypedef.h>
#include <dtkImagingSettings.h>
#include <dtkImage.h>
#include <dtkImageConverter.h>
#include <dtkImageReader.h>
#include <dtkImageWriter.h>
    //#include <dtkAbstractImageStatistics.h>
    using namespace dtk::imaging;

#include <unordered_map>

%}

%include "numpy.i"

%init %{
import_array();
%}

%feature("director");

%inline %{
    static std::unordered_map<int, int> type_to_numpy_type = {
                                                           {qMetaTypeId<          bool>(), NPY_BOOL},
                                                           {qMetaTypeId<unsigned  char>(), NPY_UBYTE},
                                                           {qMetaTypeId<          char>(), NPY_BYTE},
                                                           {qMetaTypeId<unsigned short>(), NPY_USHORT},
                                                           {qMetaTypeId<         short>(), NPY_SHORT},
                                                           {qMetaTypeId<unsigned   int>(), NPY_UINT32},
                                                           {qMetaTypeId<           int>(), NPY_INT32},
                                                           {qMetaTypeId<unsigned  long>(), NPY_UINT64},
                                                           {qMetaTypeId<          long>(), NPY_INT64},
                                                           {qMetaTypeId<         float>(), NPY_FLOAT},
                                                           {qMetaTypeId<        double>(), NPY_DOUBLE}
 };

 static std::unordered_map<int, int> numpy_type_to_type = {
                                                           {NPY_BOOL,   qMetaTypeId<          bool>()},
                                                           {NPY_UBYTE,  qMetaTypeId<unsigned  char>()},
                                                           {NPY_BYTE,   qMetaTypeId<          char>()},
                                                           {NPY_USHORT, qMetaTypeId<unsigned short>()},
                                                           {NPY_SHORT,  qMetaTypeId<         short>()},
                                                           {NPY_UINT32, qMetaTypeId<unsigned   int>()},
                                                           {NPY_INT32,  qMetaTypeId<           int>()},
                                                           {NPY_UINT64, qMetaTypeId<unsigned  long>()},
                                                           {NPY_INT64,  qMetaTypeId<          long>()},
                                                           {NPY_FLOAT,  qMetaTypeId<         float>()},
                                                           {NPY_DOUBLE, qMetaTypeId<        double>()}
 };

     int image_type_to_numpy_type(int type)
    {
        auto it = type_to_numpy_type.find(type);
        if (it != type_to_numpy_type.end()) {
            return it->second;
        } else {
            return -1;
        }
    }

   int numpy_type_to_image_type(int index)
    {
        auto it = numpy_type_to_type.find(index);
        if (it != numpy_type_to_type.end()) {
            return it->second;
        } else {
            return -1;
        }
    }

 %}

%ignore  dtkImage::origin()  const;
%ignore  dtkImage::spacing() const;

%extend dtkImage {
    static dtkImage *fromRaw(int storage_type, std::size_t x_dim, std::size_t y_dim, std::size_t z_dim, long int raw_data_add, PixelType pixel_type)
    {
        void *raw_data = reinterpret_cast<void *>(raw_data_add);
        return dtkImage::fromRawData(storage_type, x_dim, y_dim, z_dim, raw_data, pixel_type);

    }

    PyObject *array() {
        const npy_intp ndims = 3;
        npy_intp* ar_dim =  new npy_intp[ndims];
        ar_dim[0] = ($self)->xDim();
        ar_dim[1] = ($self)->yDim();
        ar_dim[2] = ($self)->zDim();
        int storage = image_type_to_numpy_type(($self)->storageType());

        PyArrayObject *array = (PyArrayObject*) PyArray_SimpleNewFromData(ndims, ar_dim, storage, static_cast<void*>(($self)->rawData()));
        return PyArray_Return(array);
    }

    PyObject *origin(void) {
        PyObject *list = PyList_New(3);
        const double *o = ($self)->origin();
        for(int i = 0 ; i < 3; ++i)  {
            PyObject* v = PyFloat_FromDouble(o[i]);
            PyList_SET_ITEM(list, i, v);
        }
        return list;
    }
    PyObject *spacing(void) {
        PyObject *list = PyList_New(3);
        const double *o = ($self)->spacing();
        for(int i = 0 ; i < 3; ++i)  {
            PyObject* v = PyFloat_FromDouble(o[i]);
            PyList_SET_ITEM(list, i, v);
        }
        return list;
    }
    void setSpacing(PyObject *o) {
        PyObject *list = static_cast<PyObject *>(o);
        if (PyList_Check(list)) {
            int end = PyList_Size(list);
            if (end != 3) {
                qWarning() << "PyList of size 3 is expected, can't setSpacing";
            }
            double *spacing = new double[3];
            for(int i = 0; i < end; ++i) {
                PyObject *o = PyList_GET_ITEM(list, i);
                spacing[i] = PyFloat_AsDouble(o);
            }
            ($self)->setSpacing(spacing);
            delete[] spacing;
        } else {
            qWarning() << "PyList if double is expected as input";
        }
    }
    void setOrigin(PyObject *o) {
        PyObject *list = static_cast<PyObject *>(o);
        if (PyList_Check(list)) {
            int end = PyList_Size(list);
            if (end != 3) {
                qWarning() << "PyList of size 3 is expected, can't setOrigin";
            }
            double *origin = new double[3];
            for(int i = 0; i < end; ++i) {
                PyObject *o = PyList_GET_ITEM(list, i);
                origin[i] = PyFloat_AsDouble(o);
            }
            ($self)->setOrigin(origin);
            delete[] origin;
        } else {
            qWarning() << "PyList if double is expected as input";
        }
    }

}

%extend QVariant {
    void setValue(dtkImage *value) {
        $self->setValue(dtk::variantFromValue(value));
    }
    dtkImage *todtkImage() const {
        return $self->value<dtkImage *>();
    }
};

// /////////////////////////////////////////////////////////////////
// Macro undefinition
// /////////////////////////////////////////////////////////////////

#undef  DTKIMAGINGCORE_EXPORT
#define DTKIMAGINGCORE_EXPORT

// /////////////////////////////////////////////////////////////////
// Wrapper input
// /////////////////////////////////////////////////////////////////

%include <dtkImaging.h>
%include <dtkImagingPixel.h>
%include <dtkImagingTypedef.h>
%include <dtkImagingSettings.h>
%include <dtkImage.h>
%include <dtkImageConverter.h>
%include <dtkImageReader.h>
%include <dtkImageWriter.h>
 //%include <dtkAbstractImageStatistics.h>

//
// dtkImagingCore.i.in ends here
